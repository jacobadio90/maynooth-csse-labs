-- SELECT * FROM lab7_enrolledon
SELECT *
FROM lab7_students AS S,
	lab7_enrolledon AS E,
	lab7_modules AS Mo
	WHERE S.studentemail ~* 'maynoothuniversity\.ie$'
		AND Mo.modulesemester = 'Semester 1'
		AND S.studentid = E.studentid
		AND Mo.moduleid = E.moduleid