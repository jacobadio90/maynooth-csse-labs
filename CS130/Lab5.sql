DROP TABLE IF EXISTS lab5_carspeed;

CREATE TABLE Lab5_CarSpeed(
	Car_ID SERIAL,
	Car_Reg VARCHAR(50) NOT NULL,
	Dir_Travel VARCHAR(100) NOT NULL,
	Num_Occupants SMALLINT NOT NULL,
	Entr_Time TIMESTAMP NOT NULL,
	Exit_Time TIMESTAMP NOT NULL,
	CONSTRAINT Car_Data PRIMARY KEY(Car_ID)
);

INSERT INTO lab5_carspeed(car_reg, dir_travel, num_occupants, entr_time, exit_time)
	VALUES
		('171-KE-2098', 'Eastbound', '3', '01-01-2017 15:15:30', '01-01-2017 15:17:50'),
		('161-CD-987', 'Westbound', '1', '13-02-2017 04:30:20', '13-02-2017 03:31:40'),
		('J19-CS130', 'Eastbound', '4', '31-05-2017 18:00:00', '31-05-2017 18:02:40'),
		('12-WD-1767', 'Westbound', '2', '07-06-2017 07:40:10', '07-06-2017 07:41:50'),
		('12-WD-1767', 'Eastbound', '2', '07-06-2017 14:59:00', '07-06-2017 15:00:50');

-- TRUNCATE lab5_carspeed;

-- DELETE FROM lab5_carspeed;

DELETE FROM lab5_carspeed
  	WHERE car_reg = '171-KE-2098';
INSERT INTO lab5_carspeed(car_reg, dir_travel, num_occupants, entr_time, exit_time)
	VALUES ('171-KE-2980', 'Eastbound', '3', '01-01-2017 15:15:30', '01-01-2017 15:17:50');
	
ALTER TABLE lab5_carspeed ADD COLUMN Speeding VARCHAR(30);
UPDATE lab5_carspeed SET Speeding  = NULL;

INSERT INTO lab5_carspeed(car_reg, dir_travel, num_occupants, entr_time, exit_time)
	VALUES ('161-KE-1234', 'Eastbound', 2, '08-11-2018 15:42:00', '08-11-2018 15:45:00'),
			('171-D-9988', 'Eastbound', 2, '08-11-2018 15:43:00', '08-11-2018 15:46:00'),
			('161-KE-1234', 'Westbound', 2, '08-11-2018 17:42:00', '08-11-2018 17:44:30'),
			('171-D-9988', 'Westbound', 2, '08-11-2018 17:43:00', '08-11-2018 17:45:30');
			
INSERT INTO lab5_carspeed(car_reg, dir_travel, num_occupants, entr_time, exit_time, Speeding)
	VALUES ('181-CN-5678', 'Eastbound', 2, '08-11-2018 15:42:00', '08-11-2018 15:42:30', NULL);

Select * from lab5_carspeed;