package Lab01;

import java.util.Scanner;

/**
 * This class performs calculations on circle.
 * It includes 2 methods for calculating area and circumference of a circle
 *
 * @author Craig Stratford
 * @version 1.0
 * @since 13 Feb 2019
 */
public class Circle {

    /**
     * PI attribute
     */
    private static double PI = 3.14159;

    /**
     * Calculate area of a circle
     *
     * @param radius double
     * @return area double
     */
    private static double area(double radius) {
        return PI * radius * radius;
    }

    /**
     * Calculate circumference of a circle
     *
     * @param radius double
     * @return circumference
     */
    private static double circumference(double radius) {
        return PI * 2 * radius;
    }

    /**
     * Main method asks user to input a radius
     * then displays to console the area and circumference
     * of the corresponding circle
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        System.out.println("Enter a radius: ");
        Scanner scanner = new Scanner(System.in);
        double radius = scanner.nextDouble();
        System.out.println("A circle of radius " + radius + " m has an area of " + Circle.area(radius) + " m^2");
        System.out.println("A circle of radius " + radius + " m has a circumference of " + Circle.circumference(radius) + " m");
        scanner.close();
    }
}