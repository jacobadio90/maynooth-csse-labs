package Lab01.atm;

import javax.swing.JOptionPane;

/**
 * This class uses a swing GUI to edit a bank account
 * It includes 2 method for starting a new transaction and for a swing GUI
 * to query the useron whether or not to withdraw/deposit and by how much
 *
 * @author Craig Stratford
 * @version 1.0
 * @since 13 Feb 2019
 */
public class Transactions {

    /**
     * is withdrawing
     * is depositing
     * amount to deposit/withdraw
     */
    private int answer1, answer2, amount;
    private boolean withdrawOK = true;
    private BankAccount ba;

    /**
     * set is withdrawing, is depositing and amount to withdraw/deposit to 0
     * create new bank account
     */
    private Transactions() {
        answer1 = 0;
        answer2 = 0;
        amount = 0;
        ba = new BankAccount(1000);
    }

    /**
     * create new transaction and start swing dialog
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        Transactions transaction = new Transactions();
        transaction.getInput();
        System.exit(0);
    }

    /**
     * Start swing application
     * User chooses to Make a Deposit (Yes/No)
     * If Yes, chooses how much money to deposit
     * If No, chooses to Make a Withdrawl (Yes/No)
     * If Yes, chooses how much money to withdraw
     * Shows user current balance
     */
    private void getInput() {
        answer1 = JOptionPane.showConfirmDialog(null, "Make a Depoist?", null, JOptionPane.YES_NO_OPTION);

        if (answer1 == JOptionPane.YES_OPTION) {
            String depString = JOptionPane.showInputDialog("Enter amount:");
            amount = Integer.parseInt(depString);

            ba.deposit(amount);


        } else if (answer1 == JOptionPane.NO_OPTION) {
            answer2 = JOptionPane.showConfirmDialog(null, "Make a Withdraw?", null, JOptionPane.YES_NO_OPTION);

            if (answer2 == JOptionPane.YES_OPTION) {
                String withString = JOptionPane.showInputDialog("Enter amount:");
                amount = Integer.parseInt(withString);

                if (!ba.withdraw(amount)) withdrawOK = false;
            }
        }
        if (!withdrawOK)
            JOptionPane.showMessageDialog(null, "Your Balance = " + ba.getBalance() + " which is not enough for this withdraw ");
        else
            JOptionPane.showMessageDialog(null, " Your balance is " + ba.getBalance());


    }
}


