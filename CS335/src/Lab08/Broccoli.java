package Lab08;

public class Broccoli extends PizzaDecorator {

    private final Pizza pizza;

    Broccoli(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDesc() {
        return pizza.getDesc() + ", Broccoli (9.25)";
    }


    @Override
    public double getPrice() {
        return pizza.getPrice() + 9.25;
    }

}
