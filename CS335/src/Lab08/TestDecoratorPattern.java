package Lab08;

import java.text.DecimalFormat;

public class TestDecoratorPattern {

    public static void main(String[] args) {

        DecimalFormat dformat = new DecimalFormat("#.##");
        Pizza pizza = new SimplyVegPizza();

        pizza = new RomaTomatoes(pizza);
        pizza = new GreenOlives(pizza);
        pizza = new Spinach(pizza);

        System.out.println("Desc: " + pizza.getDesc());
        System.out.println("Price: " + dformat.format(pizza.getPrice()));

        pizza = new SimplyNonVegPizza();

        pizza = new Meat(pizza);
        pizza = new Cheese(pizza);
        pizza = new Cheese(pizza);
        pizza = new Ham(pizza);

        System.out.println("Desc: " + pizza.getDesc());
        System.out.println("Price: " + dformat.format(pizza.getPrice()));

        MealDeals mealDealT = new TexasGrillPizza();
        System.out.println("Desc: " + mealDealT.getPizza().getDesc());
        System.out.println("Price: " + dformat.format(mealDealT.getPizza().getPrice()));

        MealDeals mealDealV = new VegetarianDelightPizza();
        System.out.println("Desc: " + mealDealV.getPizza().getDesc());
        System.out.println("Price: " + dformat.format(mealDealV.getPizza().getPrice() * .8));

        MealDeals mealDealN = new NapoliPizza();
        System.out.println("Desc: " + mealDealN.getPizza().getDesc());
        System.out.println("Price: " + dformat.format(mealDealN.getPizza().getPrice() * .8));

    }
}