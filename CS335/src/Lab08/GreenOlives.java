package Lab08;

public class GreenOlives extends PizzaDecorator {

    private final Pizza pizza;

    GreenOlives(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDesc() {
        return pizza.getDesc() + ", Green Olives (5.47)";
    }


    @Override
    public double getPrice() {
        return pizza.getPrice() + 5.47;
    }

}
