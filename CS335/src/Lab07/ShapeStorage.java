package Lab07;

class ShapeStorage {

    private Shape[] shapes = new Shape[5];
    private int index;

    void addShape(String name) {
        int i = index++;
        shapes[i] = new Shape(i, name);
    }

    Shape[] getShapes() {
        return shapes;
    }
}
