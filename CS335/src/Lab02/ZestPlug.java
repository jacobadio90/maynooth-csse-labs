package Lab02;

public final class ZestPlug implements GermanPlugConnector {
    public void giveElectricity() {
        System.out.println("giving electricity to a Zest plug.");
    }
}