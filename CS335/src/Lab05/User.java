package Lab05;

import java.util.ArrayList;

public class User {

    private static ArrayList<User> userList = new ArrayList<>();

    private String userName;
    private String level;
    private AccessControl accessControl;

    public User(String userName, String level, AccessControl accessControl) {
        this.userName = userName;
        this.level = level;
        this.accessControl = accessControl;
    }

    String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    AccessControl getAccessControl() {
        return accessControl;
    }

    public void setAccessControl(AccessControl accessControl) {
        this.accessControl = accessControl;
    }

    static User createUser(String username, String level, AccessControl userAC) {
        for (User u : userList) {
            if (u.getUserName().equals(username)) {
                System.out.println("This user already exists");
                return null;
            }
        }
        User user = new User(username, level, userAC);
        userList.add(user);
        return user;
    }

    @Override
    public String toString() {
        if (this.getLevel().equals("SUPERUSER Level")) {
            System.out.println(userList);
        } else {
            System.out.println("No Permission to Print");
        }
        return "";
    }

}
