package Lab05;

public class TestPrototypePattern {

    public static void main(String[] args) {
        AccessControl userAccessControl = AccessControlProvider.getAccessControlObject("USER");
//        User user = new User("User A", "USER Level", userAccessControl);
        User user = User.createUser("User A", "USER Level", userAccessControl);

        System.out.println("************************************");

        if (user != null) {
            System.out.println(user);
        }
        System.out.println("************************************");


        userAccessControl = AccessControlProvider.getAccessControlObject("USER");
        user = User.createUser("User B", "USER Level", userAccessControl);
        if (user != null) {
            System.out.println("Changing access control of: " + user.getUserName());
            user.getAccessControl().setAccess("READ REPORTS");
            System.out.println(user);
        }
        System.out.println("************************************");

        AccessControl managerAccessControl = AccessControlProvider.getAccessControlObject("MANAGER");
        user = User.createUser("User C", "MANAGER Level", managerAccessControl);
        if (user != null) {
            System.out.println(user);
        }
        System.out.println("************************************");

        userAccessControl = AccessControlProvider.getAccessControlObject("SUPERUSER");
        user = User.createUser("User D", "SUPERUSER Level", userAccessControl);
        if (user != null) {
            System.out.println("Changing access control of: " + user.getUserName());
            user.getAccessControl().setAccess("ADD/REMOVE USERS & INSTALL/UNINSTALL APPLICATIONS");
//            User.toString(user);
        }
    }
}
