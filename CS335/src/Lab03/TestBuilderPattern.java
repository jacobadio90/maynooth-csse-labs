package Lab03;

public class TestBuilderPattern {

    public static void main(String[] args) {
        CarBuilder carBuilder = new SedanCarBuilder();
        CarDirector director = new CarDirector(carBuilder);
        director.build();
        Car car = carBuilder.getCar();
        System.out.println(car);

        System.out.println("\n\n");

        SportsCarBuilder ScarBuilder = new SportsCarBuilder();
        CarDirector Sdirector = new CarDirector(ScarBuilder);
        Sdirector.build();
        Car Scar = ScarBuilder.getCar();
        System.out.println(Scar);

    }


}
