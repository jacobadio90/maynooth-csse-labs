package whatevenismyliferightnowpleasekillmethislabisawful;

import java.util.ArrayList;
import java.util.Scanner;

public class sweetMultiplication {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sweetMultiplication sm = new sweetMultiplication();
        int x = sc.nextInt();
        int[] pf = sm.findPrimeFactors(x);
    }

    public int[] findPrimeFactors(int n) {
        ArrayList<Integer> ar = new ArrayList<>();
        for (int i = 2; i < n / 2 + 1; i++) {
            while (n % i == 0){
                ar.add(i);
                n /= i;
            }
        }
        return ArrayListToArray(ar);
    }

    public int[] ArrayListToArray(ArrayList<Integer> ar){
        int[] a = new int[ar.size()];
        for(int i = 0; i < ar.size(); i++){
            a[i] = ar.get(i);
        }
        return a;
    }

//    public void findMultiplicativePartitions() {
//        for (int i = 1; i < factors.size; i++) {
//            for (List<int> subset : factors.permutate(2)) {
//                List<int> otherSubset = factors.copy().remove(subset);
//                int subsetTotal = 1;
//                for (int p : subset) subsetTotal *= p;
//                int otherSubsetTotal = 1;
//                for (int p : otherSubset) otherSubsetTotal *= p;
//                // assume your partition excludes if it's a duplicate
//                partition.add(new FactorSet(subsetTotal, otherSubsetTotal));
//            }
//        }
//    }

}
