package stack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class intStack {

    private List<Integer> stack;

    public intStack() {
        stack = new ArrayList<Integer>();
    }

    public int pop() {
        if (!stack.isEmpty()) return stack.remove(0);
        else return -1;
    }

    public int push(int i) {
        stack.add(i);
        return 0;
    }

    public int peek() {
        if (!stack.isEmpty()) return stack.get(0);
        else return -1;
    }

    public int max() {
        return Collections.max(stack);
    }

    public int min() {
        return Collections.min(stack);
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }
}
