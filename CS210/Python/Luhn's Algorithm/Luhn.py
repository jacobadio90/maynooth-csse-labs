number = "79927398713"

def luhnGetFinalNumber(number):
    ccNumber = list(number[::-1])
    sum = 0
    for x in range(1, len(ccNumber)):
        val = int(ccNumber[x])
        if x % 2 == 1:
            val *= 2
            if val > 9:
                val -= 9
        sum += val
    return 10 - sum % 10


def luhnValid(number):
    ccNumber = list(number[::-1])
    sum = 0
    for x in range(0, len(ccNumber)):
        val = int(ccNumber[x])
        if x % 2 == 1:
            val *= 2
            if val > 9:
                val -= 9
        sum += val
    return sum % 10 == 0


print(luhnGetFinalNumber(number))
print(luhnValid(number))
