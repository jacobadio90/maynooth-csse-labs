package tk.imcraig.sorting;

import tk.imcraig.sorting.unsorted.readNumstoArray;

public class BubbleSort {
    public static void main(String[] args) {

        readNumstoArray rna = new readNumstoArray();
        BubbleSort bs = new BubbleSort();
        for (int i : bs.bubbleSort(rna.readFile("100"))) {
            System.out.println(i);
        }
    }

    public int[] bubbleSort(int[] ar) {
        for (int i = 0; i < ar.length - 1; i++) {
            for (int j = 0; j < ar.length - i - 1; j++) {
                if (ar[j] > ar[j + 1]){
                    int temp = ar[j];
                    ar[j] = ar[j+1];
                    ar[j+1] = temp;
                }
            }
        }
        return ar;
    }

}
