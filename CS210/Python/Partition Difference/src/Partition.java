class Partition {

    // Function to find the minimum sum 
    public static int findMinRec(int arr[], int i,
                                 int sumCalculated,
                                 int sumTotal) {
        // If we have reached last element. 
        // Sum of one subset is sumCalculated, 
        // sum of other subset is sumTotal- 
        // sumCalculated.  Return absolute  
        // difference of two sums. 
        if (i == 0)
            return Math.abs((sumTotal - sumCalculated) -
                    sumCalculated);


        // For every item arr[i], we have two choices 
        // (1) We do not include it first set 
        // (2) We include it in first set 
        // We return minimum of two choices 
        return Math.min(findMinRec(arr, i - 1, sumCalculated
                        + arr[i - 1], sumTotal),
                findMinRec(arr, i - 1,
                        sumCalculated, sumTotal));
    }

    // Returns minimum possible difference between 
    // sums of two subsets 
    public static int findMin(int arr[], int n) {
        // Compute total sum of elements 
        int sumTotal = 0;
        for (int i = 0; i < n; i++)
            sumTotal += arr[i];

        // Compute result using recursive function 
        return findMinRec(arr, n, 0, sumTotal);
    }

    /* Driver program to test above function */
    public static void main(String[] args) {
        int arr[] = {3314, 2417, 6502, 2708, 4716, 1173, 9949, 7145, 5535, 4677, 3272, 4488, 9367, 4745, 6131, 1554, 4682, 4781, 3577, 9202, 1703, 3487, 8297, 6692, 1819, 1604, 6275, 5387, 6460, 8147, 6745, 7069, 1884, 5200, 6944, 4946, 6683, 3423, 2680, 9809, 3760, 7160, 2499, 2463, 6588, 8356, 153, 2443, 7240, 1540, 9722, 3269, 4497, 48, 4901, 9996, 7749, 1666, 4957, 7854, 4699, 3772, 6554, 6088};
        int n = arr.length;
        System.out.print("The minimum difference" +
                " between two sets is " +
                findMin(arr, n));
    }
} 