package tk.imcraig.labs210;

import java.util.Scanner;

public class lab02_colatzConjecture {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        long num = sc.nextLong();
        int count = 0;
        while (num != 1){
            if(num %2 == 0) num /= 2;
            else num = (num * 3) + 1;
            System.out.println(num);
            count++;
        }
        System.out.println(count);

    }
}
