package tk.imcraig.cs162.Lab02;

import java.util.Scanner;

public class MethodsArrays {
    public static void main(String[] args) {
        int[] x = fillArray();
        System.out.println("Sum = " + sumArray(x));
        System.out.println("Average = " + avgArray(x));
        printArray(x);
    }

    private static int[] fillArray() {
        Scanner sc = new Scanner(System.in);
        int[] x = new int[sc.nextInt()];
        for (int i = 0; i < x.length; i++) {
            x[i] = sc.nextInt();
        }
        return x;
    }

    private static int sumArray(int[] x) {
        int y = 0;
        for (int x1 : x) {
            y += x1;
        }
        return y;
    }

    private static double avgArray(int[] x) {
        double y = 0;
        for (int x1 : x) {
            y += x1;
        }
        return y / x.length;
    }

    private static void printArray(int[] x) {
        for (int x1 : x) {
            System.out.print(x1 + " ");
        }
    }

}