package tk.imcraig.cs162.Lab03;

import java.util.Scanner;

public class WhichExp {
    public static void main(String[] args) {
        WhichExp WhichExp = new WhichExp();
        Scanner sc = new Scanner(System.in);
        WhichExp.match(sc.next(), "a", "b");
    }

    private void match(String s, String a, String b) {
        boolean t = false;
        if (s.matches("a([ba])b")) {
            System.out.println("1");
            t = true;
        }
        if (s.matches("(ab)*b")) {
            System.out.println("2");
            t = true;
        }
        if (s.matches("a([ba])*")) {
            System.out.println("3");
            t = true;
        }
        if (s.matches("(([ab])a)*")) {
            System.out.println("4");
            t = true;
        }
        if (!t) System.out.println("Not in the language");
    }

}