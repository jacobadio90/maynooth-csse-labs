package tk.imcraig.cs210.Lab07_Stack;

import java.util.Scanner;

public class StackDirections {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Stack stack = new Stack();
        String inp = sc.nextLine();
        while (!inp.matches("Arrived")) {
            if (inp.matches("Go Back")) stack.pop();
            else stack.push(inp.split(" ")[1]);
            inp = sc.nextLine();
        }
        while (!stack.isEmpty()) {
            if (stack.pop().equalsIgnoreCase("North")) System.out.println("Go South");
            else if (stack.pop().equalsIgnoreCase("South")) System.out.println("Go North");
            else if (stack.pop().equalsIgnoreCase("East")) System.out.println("Go West");
            else System.out.println("Go East");
        }
    }
}

class Node {
    String data;
    Node next;

    Node(String data) {
        this.data = data;
    }
}

class Stack {
    private Node head;

    void push(String data) {
        if (head == null) {
            head = new Node(data);
            return;
        }
        Node newHead = new Node(data);
        newHead.next = head;
        head = newHead;
    }

    String pop() {
        if (head == null) return "";
        Node returnHead = head;
        head = head.next;
        return returnHead.data;
    }

    boolean isEmpty() {
        return head == null;
    }

}