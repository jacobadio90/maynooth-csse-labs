package tk.imcraig.cs210.Lab05_SortWords;

import java.util.Scanner;

public class SortWords {
    public static void main(String[] args) {
        SortWords sw = new SortWords();
        String[] ar = sw.fillArray();
        ar = sw.sort(ar);

        for (String i : ar) {
            System.out.println(i);
        }

    }

    private String[] fillArray() {
        Scanner sc = new Scanner(System.in);
        String[] ar = new String[sc.nextInt()];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = sc.next();
        }
        return ar;
    }

    private String[] sort(String[] ar) {

        SortWords sw = new SortWords();
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar.length - 1 - i; j++) {
                if (ar[j].length() > ar[j + 1].length()) {
                    ar = sw.swap(j, j + 1, ar);
                }
                if (ar[j].length() == ar[j + 1].length() && ar[j].compareToIgnoreCase(ar[j + 1]) > 0) {
                    ar = sw.swap(j, j + 1, ar);
                }
            }
        }
        return ar;

    }

    private String[] swap(int a, int b, String[] ar) {
        String temp = ar[a];
        ar[a] = ar[b];
        ar[b] = temp;
        return ar;
    }

}
