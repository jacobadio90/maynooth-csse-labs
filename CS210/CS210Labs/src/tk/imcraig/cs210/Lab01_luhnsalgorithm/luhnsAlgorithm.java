package tk.imcraig.cs210.Lab01_luhnsalgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class luhnsAlgorithm {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long ccNumber = sc.nextInt();
        sc.close();
        ArrayList<Integer> ar = new ArrayList<>();
        for (String c : String.valueOf(ccNumber).split("")) {
            ar.add(Integer.parseInt(c));
        }
        Collections.reverse(ar);

        for (int i = 0; i < ar.size(); i++) {
            if (i % 2 == 1) {
                ar.set(i, ar.get(i) * 2);
                if (ar.get(i) > 9) {
                    ar.set(i, ar.get(i) - 9);
                }
            }
        }
        int sum = 0;
        for (Integer i : ar) {
            sum += i;
        }
        if (sum % 10 != 0)
            System.out.println(false);
        else
            System.out.println(true);
    }
}
