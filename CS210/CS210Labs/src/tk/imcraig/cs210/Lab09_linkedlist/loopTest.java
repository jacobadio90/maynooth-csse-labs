package tk.imcraig.cs210.Lab09_linkedlist;

import java.util.Scanner;

public class loopTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = Integer.parseInt(sc.nextLine());
        Link[] array = new Link[num];
        for (int i = 0; i < num; i++) {
            array[i] = new Link(sc.nextLine());
        }
        for (int i = 0; i < num; i++) {
            int select = sc.nextInt();
            int next = sc.nextInt();
            if (next != -1) {
                array[select].next = array[next];
            }
        }
        LinkedList mylist = new LinkedList();
        if (num > 0) {
            mylist.first = array[0];
        }
        System.out.println(findLoopLength(mylist));
    }

    private static int findLoopLength(LinkedList mylist) {
        if (mylist.isEmpty()) {
            return (0);
        }
        Link[] checklist = new Link[100];
        int counter = 0;
        Link forwards = mylist.first;
        while (forwards.next != null) {
            checklist[counter] = forwards;
            for (int i = 0; i < counter; i++) {
                if (forwards == checklist[i]) {
                    return (counter - i);
                }
            }
            forwards = forwards.next;
            counter++;
        }
        return 0;
    }
}

class Link {
    private String data;
    public Link next;

    Link(String input) {
        data = input;
        next = null;
    }
}

class LinkedList {
    Link first;

    LinkedList() {
        first = null;
    }

    boolean isEmpty() {
        return (first == null);
    }

    public void insertHead(Link insert) {
        if (isEmpty()) {
            first = insert;
        } else {
            insert.next = first;
            first = insert;
        }
    }
}