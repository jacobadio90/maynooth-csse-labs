package tk.imcraig.cs210.sortingalgorithms;

import tk.imcraig.cs210.sortingalgorithms.unsorted.readNumstoArray;


public class selectionSort {
    public static void main(String[] args) {

        readNumstoArray rna = new readNumstoArray();
        selectionSort ss = new selectionSort();
        for (int i : ss.selSort(rna.readFile("100"))) {
            System.out.println(i);
        }
    }

    private int[] selSort(int[] ar) {

        // One by one move boundary of unsorted subarray
        for (int i = 0; i < ar.length - 1; i++) {
            // Find the minimum element in unsorted array
            int min = i;
            for (int j = i + 1; j < ar.length; j++)
                if (ar[j] < ar[min])
                    min = j;

            // Swap the found minimum element with the first
            // element
            int temp = ar[min];
            ar[min] = ar[i];
            ar[i] = temp;
        }
        return ar;

    }
}
