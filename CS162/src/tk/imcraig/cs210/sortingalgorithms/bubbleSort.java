package tk.imcraig.cs210.sortingalgorithms;

import tk.imcraig.cs210.sortingalgorithms.unsorted.readNumstoArray;


public class bubbleSort {
    public static void main(String[] args) {

        readNumstoArray rna = new readNumstoArray();
        bubbleSort bs = new bubbleSort();
        for (int i : bs.bubSort(rna.readFile("100"))) {
            System.out.println(i);
        }
    }

    private int[] bubSort(int[] ar) {
        for (int i = 0; i < ar.length - 1; i++) {
            for (int j = 0; j < ar.length - i - 1; j++) {
                if (ar[j] > ar[j + 1]) {
                    int temp = ar[j];
                    ar[j] = ar[j + 1];
                    ar[j + 1] = temp;
                }
            }
        }
        return ar;
    }

}
