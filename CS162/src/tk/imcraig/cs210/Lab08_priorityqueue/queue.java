package tk.imcraig.cs210.Lab08_priorityqueue;

import java.util.Scanner;

public class queue {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        PQueue que = new PQueue(size);
        for (int i = 0; i < size + 1; i++) {
            String x = sc.nextLine();
            if (x.split("")[0].equalsIgnoreCase("INSERT")) que.insert(x.split(" ")[1]);
            else que.remove();
        }
        System.out.println(que.peekFront());
    }
}

class PQueue {
    private int maxSize;
    private String[] queArray;
    private int front = 0;
    private int nitems = 0;

    PQueue(int s) {          // constructor
        maxSize = s;
        queArray = new String[maxSize];
        front = -1;
    }

    void insert(String item) {            // insert item
        if (nitems == 0) {
            queArray[0] = item;
        } else {
            int j = nitems;
            while (j > 0 && queArray[j - 1].length() > item.length() && compare(queArray[j - 1], item)) {
                queArray[j] = queArray[j - 1];
                j--;
            }
            queArray[j] = item;
        }
        nitems++;

    }

    void remove() {         // take item from front of queue
        if (isEmpty()) return;
        String temp = queArray[front];
        front++;
        if (front == maxSize) front = 0;
        nitems--;
    }

    String peekFront() {    // peek at front of queue
        if (isEmpty()) {
            return null;
        }
        return queArray[front];
    }

    private boolean isEmpty() {    // true if queue is empty
        return nitems == 0;
    }

    public boolean isFull() {    // true if queue is full
        return nitems == maxSize;
    }

    private boolean compare(String j, String item) {
        return j.compareTo(item) >= 0;
    }

}