package lab05_EXAM;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LabExam1_Q2_TEST {
	
	
//	/********************/
//	/*	QUESTION 1(a)	*/
//	/********************/
	private static Object[][] testData1a = new Object[][] {
		//test,	speed,	expected
		{"T2a.1", null,					"INVALID"},
		{"T2a.2", "",					"INVALID"},
		{"T2a.3", "19.1",				"INVALID"},
		{"T2a.4", "192.456.789.012",	"LOCAL"},
		{"T2a.5", "127.0.0.1",			"LOCAL"},
		{"T2a.6", "123.456.789.012",	"EXTERNAL"},
		{"T2a.7", "123.456.789.012.000.111.222",	"INVALID"},
	};

	@DataProvider(name = "Q1a")
	public Object[][] getTestData1a(){
		return testData1a;
	}

	@Test(dataProvider = "Q1a")
	public void test_GetSpeed1a(String id, String ip, String expected) {
		assertEquals(LabExam1_Q2.isIPAddress(ip), expected);
	}


}
