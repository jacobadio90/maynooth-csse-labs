package lab04;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class IntroWBTTest {

	@Test
	public void Ex01Test01() {
		boolean expected = true;
		assertEquals(expected, IntroWBT.isPalindrome("HellolleH"));
	}

	@Test
	public void Ex01Test02() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome("Hello olleH"));
	}

	@Test
	public void Ex01Test03() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome("1234567890"));
	}

	@Test
	public void Ex01Test04() {
		boolean expected = true;
		assertEquals(expected, IntroWBT.isPalindrome("Navan"));
	}

	@Test
	public void Ex01Test05() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome("#@#~#@#"));
	}

	@Test
	public void Ex01Test06() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome("Test1234##4321tseT"));
	}

	@Test
	public void Ex01Test07() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome("HELP ME IM DYING INSIDE ALL THE TIME"));
	}

	@Test
	public void Ex01Test08() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome(""));
	}

	@Test
	public void Ex01Test09() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome(null));
	}

	@Test
	public void Ex01Test10() {
		boolean expected = false;
		assertEquals(expected, IntroWBT.isPalindrome("How many test cases do I need here ereh deen I od sesac tset ynam woH"));
	}

}
