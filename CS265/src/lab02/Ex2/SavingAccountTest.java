package lab02.Ex2;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class SavingAccountTest {

	double delta = 0.00000001;
	
	@Test
	public void Ex2_Test1() {
		double expectedValue = 0.00;
		assertEquals(expectedValue, SavingAccount.discountRate(-100), delta);
	}
	@Test
	public void Ex2_Test2() {
		double expectedValue = 0.03;
		assertEquals(expectedValue, SavingAccount.discountRate(55), delta);
	}
	@Test
	public void Ex2_Test3() {
		double expectedValue = 0.05;
		assertEquals(expectedValue, SavingAccount.discountRate(600), delta);
	}
	@Test
	public void Ex2_Test4() {
		double expectedValue = 0.07;
		assertEquals(expectedValue, SavingAccount.discountRate(2500), delta);
	}
}
