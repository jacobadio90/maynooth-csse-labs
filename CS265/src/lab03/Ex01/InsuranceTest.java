package lab03.Ex01;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class InsuranceTest {
  @Test
  public void Ex1Test01() {
	  Insurance ins = new Insurance();
	  int expected = 5;
	  assertEquals(expected, ins.equipmentInsurance(false, false));
  }
  
  @Test
  public void Ex1Test02() {
	  Insurance ins = new Insurance();
	  int expected = 10;
	  assertEquals(expected, ins.equipmentInsurance(true, false));
  }
  
  @Test
  public void Ex1Test03() {
	  Insurance ins = new Insurance();
	  int expected = 10;
	  assertEquals(expected, ins.equipmentInsurance(false, true));
  }
  
  @Test
  public void Ex1Test04() {
	  Insurance ins = new Insurance();
	  int expected = 20;
	  assertEquals(expected, ins.equipmentInsurance(true, true));
  }
  
}
