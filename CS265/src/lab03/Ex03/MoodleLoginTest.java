package lab03.Ex03;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class MoodleLoginTest {
	@Test
	public void Ex3Test01() {
		String expected = "UNKNOWN";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234", "a", false));
	}

	@Test
	public void Ex3Test02() {
		String expected = "UNKNOWN";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234", "a", true));
	}

	@Test
	public void Ex3Test03() {
		String expected = "UNKNOWN";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234", "1234Abc", false));
	}

	@Test
	public void Ex3Test04() {
		String expected = "UNKNOWN";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234", "1234Abc", true));
	}

	@Test
	public void Ex3Test05() {
		String expected = "UNKNOWN";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234@mumail.ie", "a", false));
	}

	@Test
	public void Ex3Test06() {
		String expected = "UNKNOWN";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234@mumail.ie", "a", true));
	}

	@Test
	public void Ex3Test07() {
		String expected = "STUDENT PAGE";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234@mumail.ie", "1234Abc", false));
	}

	@Test
	public void Ex3Test08() {
		String expected = "STAFF PAGE";
		assertEquals(expected, MoodleLogin.doMoodleLogon("Test1234@mumail.ie", "1234Abc",true));
	}

}
