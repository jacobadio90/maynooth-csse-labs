package tk.imcraig.lab09.prioritiseReader;


class DataAccessPolicyManager {
    private int readerCount;

    private Semaphore mutex;

    private Semaphore wrt;

    DataAccessPolicyManager() {
        readerCount = 0;
        mutex = new Semaphore(1);
        wrt = new Semaphore(1);
    }

    void acquireReadLock() {
        mutex.acquire();
        ++readerCount;
        if (readerCount == 1) // This is the first reader
            wrt.acquire();
        mutex.release();
    }

    void releaseReadLock() {
        mutex.acquire();
        --readerCount;
        if (readerCount == 0) // Last reader
            wrt.release();
        mutex.release();
    }

    void acquireWriteLock() {
        wrt.acquire();
    }

    void releaseWriteLock() {
        wrt.release();
    }
}