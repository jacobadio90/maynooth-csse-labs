package tk.imcraig.lab09.prioritiseReader;

public class Writer extends Thread {
    DataAccessPolicyManager lockManager;
    int id;

    Writer(DataAccessPolicyManager lockManager, int id) {
        this.lockManager = lockManager;
        this.id = id;
    }

    public void run() {
        while (true) {
            lockManager.acquireWriteLock();
            System.out.println("Writer " + id + " waiting.");
            try { // Simulate eating activity for a random time
                sleep((int) (Math.random() * 10000));
            } catch (InterruptedException ignored) {
            }
            lockManager.releaseWriteLock();
        }

    }
}