package tk.imcraig.Lab07;

/* The tk.imcraig.Lab07.Semaphore class contains methods declared as
synchronized. Java's locking mechanism will ensure
that access to tk.imcraig.Lab07.Semaphore methods is mutually exclusive
among threads that invoke these methods.
*/
class Semaphore {
	private int value;
	
	Semaphore(int value) {
		this.value = value;
	}

	synchronized void acquire() {
		while (value == 0) {
			try {
				// Calling thread waits until semaphore is free
				wait();
			} catch(InterruptedException ignored) {}
		}
		value = value - 1;
	}

	synchronized void release() {
		value = value + 1;
		notify();
	}
}