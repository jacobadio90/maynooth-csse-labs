package tk.imcraig.lab08;

/* The Semaphore class contains methods declared as
synchronized. Javas locking mechanism will ensure
that access to Semaphore methods is mutually exclusive
among threads that invoke these methods.
*/
class Semaphore {
	private int value;
	
	Semaphore(int value) {
		this.value = value;
	}

	synchronized void acquire() {
		while (value == 0) {
			try {
				// Calling thread waits until semaphore is free
				wait();
			} catch(InterruptedException ignored) {}
		}
		value = value - 1;
	}

	synchronized void release() {
		value = value + 1;
		notify();
	}
}