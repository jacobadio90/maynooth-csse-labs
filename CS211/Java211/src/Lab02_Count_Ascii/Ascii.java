package Lab02_Count_Ascii;

import java.util.Scanner;

public class Ascii {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = sc.nextLine();

        Ascii ascii = new Ascii();
        ascii.toAscii(s1);
        ascii.countChars(s1);

        sc.close();
    }

    private void toAscii(String s) {
        for (char c : s.toCharArray()) {
            System.out.println(String.format("%07" + Integer.parseInt(Integer.toBinaryString(c))));
        }
    }

    private void countChars(String s) {
        int[] ar = new int[128];
        for (int a : ar){
            a = 0;
        }
        for (char c : s.toCharArray()){
            ar[c] += 1;
        }
        for (int i = 0; i < 128; i++) {
            if(ar[i] != 0){
                System.out.println((char) i + " occurs " + ar[i] + " times");
            }
        }
    }

}
