import locations as loc
import mplleaflet
import matplotlib.pyplot as plt
from haversine import haversine as hs


plt.figure(figsize=(50, 50))


latitude_list = loc.get_lat_list(None)
longitude_list = loc.get_long_list(None)

col = 1 / len(latitude_list)

for x in range(len(latitude_list)):
    plt.scatter(longitude_list[x], latitude_list[x])
plt.plot([-6.59192] + longitude_list, [53.38195] + latitude_list)
plt.scatter(-6.59192, 53.38195, s=100)

offset = .00005
for i in range(len(latitude_list)):
    plt.annotate(i + 1, (longitude_list[i] + offset, latitude_list[i] + offset))

mplleaflet.show()


def original_route_time():
    import csv
    total = 0
    data = list(csv.reader(open('distHrs.csv')))
    for row in range(1, 101):
        for column in range(1, 101):
            if row + 1 == column:
                total += float(data[row][column])
    print('Original Route:', total)


# original_route_time()

def to_distance():
    import csv
    data = []
    for xlat, xlon, _ in loc.get_locs_latlon():
        d1 = []
        for ylat, ylon, _ in loc.get_locs_latlon():
            d1.append(hs((xlat, xlon), (ylat, ylon)))
        data.append(d1)
    with open('distances.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)


def to_time():
    import csv
    data = list(csv.reader(open('distances.csv')))
    for indexx, x in enumerate(data):
        for indexy, y in enumerate(x):
            data[indexx][indexy] = float(data[indexx][indexy]) / 80

    with open('time.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)


def to_time_hrs():
    import csv
    data = list(csv.reader(open('time.csv')))
    for indexx, x in enumerate(data):
        for indexy, y in enumerate(x):
            if indexy > 0 and indexx > 0:
                data[indexx][indexy] = float(data[indexx][indexy]) * 60

    with open('distHrs.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)


def to_time_mins():
    import csv
    data = list(csv.reader(open('time.csv')))
    for indexx, x in enumerate(data):
        for indexy, y in enumerate(x):
            data[indexx][indexy] = float(data[indexx][indexy]) * 60

    with open('distMins.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)


# to_time()
# to_distance()
# to_time_hrs()
# to_time_mins()


def greedy_solve():
    row = 0
    visited = [row]
    time = 0
    import csv
    with open('distHrs.csv') as csvfile:
        reader = csv.reader(csvfile)
        time_list = list(reader)
    for x in range(len(time_list)):  # remove diagonals
        time_list[x].pop(x)

    while len(visited) < len(loc.get_locs_latlon()):
        min_index = 0
        min_val = 999
        for index, x in enumerate(time_list[row]):
            if float(x) < float(min_val) and index not in visited:
                min_val = x
                min_index = index
        time += float(min_val)
        row = min_index
        visited.append(min_index)
    print(visited)
    print("Greedy Route:", time)


# greedy_solve()

# print(hs((53.3473, -6.55057), (53.37077, -6.48279)) / 80)
