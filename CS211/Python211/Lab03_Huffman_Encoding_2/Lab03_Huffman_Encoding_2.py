class Node:

    def __init__(self, data, weight):
        self.left = None
        self.right = None
        self.data = data
        self.weight = weight

    # Print every node
    def print_tree(self):
        if self.left:
            self.left.print_tree()
        print(self.data, self.weight)
        if self.right:
            self.right.print_tree()

    # Print every node with parents and children
    def print_tree_data(self):
        if self.left:
            print("pare", self.data, self.weight)
            print("left", self.left.data, self.left.weight)
            self.left.print_tree()
        print(self.data, self.weight)
        if self.right:
            print("pare", self.data, self.weight)
            print("righ", self.right.data, self.right.weight)
            self.right.print_tree()

    def smallest(self, root):
        small = 128
        if root:
            if ord(root.data) < small:
                small = ord(root.data)
            res = self.smallest(root.left)
            res.append(root.data)
            res = res + self.smallest(root.right)
        return small


# Convert a string to binary tree
def to_tree(inp):
    y = sorted([[x, inp.count(x)] for x in set(inp)], key=lambda x: x[1])
    for x in range(len(y)):
        y[x] = Node(y[x][0], y[x][1])

    while len(y) > 1:
        y = sorted(y, key=lambda x: (x.weight))  # , ord(x.data) if x.data else 128))
        root = Node(data=None, weight=y[0].weight + y[1].weight)
        if root.left and root.right:
            if root.left.smallest() < root.right.smallest() and root.left.weight == root.right.weight:
                root.left = y[0]
                root.right = y[1]
            elif root.left.smallest() > root.right.smallest() and root.left.weight == root.right.weight:
                root.right = y[0]
                root.left = y[1]
            else:
                root.left = y[0]
                root.right = y[1]
        else:
            root.left = y[0]
            root.right = y[1]

        y[0] = root
        del (y[1])

        # y[0].print_tree_data()
    return y[0]


codes = {}


def code(s, node):
    if node.data:
        if not s:
            codes[node.data] = "0"
        else:
            codes[node.data] = s
    else:
        code(s + "0", node.left)
        code(s + "1", node.right)
    return codes


inp = input()

Huffcodes = code("", to_tree(inp))
print(Huffcodes)

for x in inp:
    print(Huffcodes.get(x), end="")
