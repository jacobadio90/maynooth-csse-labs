from math import sin, asin, sqrt, cos, radians


class Haversine:

    def __init__(self, location1, location2):

        try:
            x = tuple(map(float, location1))
            y = tuple(map(float, location2))
            del x
            del y
        except ValueError:
            print("Value(s) entered not a valid number")
            exit(1)

        self.loc1 = location1
        self.loc2 = location2
        self.distance = None
        self.time = None
        self.speed = 80.0

    def __str__(self):
        return "Distance: " + str(self.distance) + \
               "\nTime:" + str(self.time) + \
               "\nSpeed:" + str(self.speed)

    def haversine(self):
        lat1, lon1 = self.loc1
        lat2, lon2 = self.loc2

        # Convert to Radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        self.distance = 2 * asin(sqrt(a)) * 6371
        self.time = self.distance / self.speed
        return self.distance

    def get_distance(self):
        return self.distance

    def get_time(self):
        return self.distance / self.speed

    def get_speed(self):
        return self.speed

    def set_speed(self, speed):
        try:
            self.speed = float(speed)
            return True
        except ValueError:
            print("Speed entered not a valid number")
            exit(1)


def main():
    sl = (40.6892, 74.0445)
    nd = (48.8530, 2.3499)

    hv = Haversine(sl, nd)
    hv.speed = 80
    hv.haversine()
    print(hv)
    print()
    hv.set_speed(90)
    hv.haversine()
    print(hv)


if __name__ == '__main__':
    main()
