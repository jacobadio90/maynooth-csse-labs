<?php

$id = 0;
if (isset($_POST['id']))
    $id = $_POST['id'];

$file = "../../data.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

$deleted = false;

foreach ($array as $key => $item) {
    if ($item['id'] == $id) {
        unset($array[$key]);
        $deleted = true;
    }
}

if (!$deleted) {
    http_response_code(409);
    return;
}

$json = json_encode($array);
file_put_contents($file, $json);

http_response_code(204);
return;