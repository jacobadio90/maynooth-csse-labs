<?php

$name = '';
if (isset($_GET['name']))
    $name = $_GET['name'];

$file = "../../../data.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

$str = "{";

$found = false;

foreach ($array as $key => $item) {
    if ($key == $name) {
        $str .= "\"" . $key . "\":" . json_encode($array[$key]) . ",";
        $found = true;
    }
}
$str .= '}';
$str = preg_replace('/,([^,]*)$/', '\1', $str);


echo $str;

if (!$found) {
    http_response_code(404);
    return;
}

http_response_code(200);
return;