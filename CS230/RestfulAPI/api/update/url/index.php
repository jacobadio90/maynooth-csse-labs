<?php

$id = 0;
$name = "";
if (isset($_POST['id']))
    $id = $_POST['id'];
if (isset($_POST['url']))
    $url = $_POST['url'];

$file = "../../../data.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

$found = false;

foreach ($array as $key => $item) {
    if ($item['id'] == $id) {
        $array[$key]['url'] = $url;
        $found = true;
    }
}

if (!$found) {
    http_response_code(404);
    return;
}

$json = json_encode($array);
file_put_contents($file, $json);

http_response_code(204);
return;