<?php

if (isset($_POST['desc']))
    $desc = $_POST['desc'];
if (isset($_POST['name']))
    $name = $_POST['name'];
if (isset($_POST['url']))
    $url = $_POST['url'];

$file = "../../data.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

$id = ++end($array)['id'];
$results = ['id' => $id, 'desc' => $desc, 'url' => $url, 'theDate' => date('d-m-Y')];


if (isset($array[$name])) {
    http_response_code(409);
    return;
}

$array[$name] = $results;
$json = json_encode($array);
file_put_contents($file, $json);

http_response_code(201);
return;