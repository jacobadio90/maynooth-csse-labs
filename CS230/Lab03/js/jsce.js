/*
1.Use JavaScript to automatically re-calculate all Final Grade averages whenever a table cell is updated by a user (1 point).
    Done


2.Use JavaScript to count the total number of assignments that have not yet been submitted, and present in styled format, beneath the table.
Furthermore, change the background of cells containing '-' (unsubmitted assignment) to yellow.
These functionality may be an automatic calculation, or achieved using a styled button (1 point)
    Done


3.Use JavaScript to automatically validate cell data when manually updated by a user.
Cells containing erroneous data should default to being “unsubmitted”, i.e. contain a '-' (1 point).
    It is now impossible to enter invalid data.
        Hyphens are automatically removed when the table cell is typed in and are added when the cell is empty
        You cannot enter a number bigger than 100, or less than 0
        You can not enter any value that doesnt correspond to a floating point number.
            i.e. /^\d+.?\d*$/
                At least one digit, one or no decimal points, and then zero or more digits
                Letters and symbols (except '.') are removed


4.Use JavaScript to write a CSV representation of the table to a styled TEXTAREA below the table
(this facilitates copying and pasting of the table data into another application, saving, etc.).
This may be an automatic calculation, or achieved using a styled button (2 points).
    Done
*/

function calculateGrades() {
    const table = document.getElementById('grades');
    for (let i = 1; i < table.rows.length; i++) {
        let avg = 0;
        for (let j = 2; j < table.rows[i].cells.length - 1; j++) {
            if (/^\d+.?\d*$/.test(table.rows[i].cells[j].innerHTML)) {
                avg += parseFloat(table.rows[i].cells[j].innerHTML);
            }
        }
        table.rows[i].cells[7].innerHTML = Math.round(avg / 5).toString() + '%';
    }
    Array.prototype.forEach.call(document.querySelectorAll('.finalGrade'), function (item) {
        if (Number(item.innerHTML.replace('%', '')) < 40) {
            item.style.backgroundColor = 'red';
            item.style.color = 'white';
        }
        if (Number(item.innerHTML.replace('%', '')) >= 40) {
            item.style.backgroundColor = 'white';
            item.style.color = 'black';
        }

    });
    Array.prototype.forEach.call(document.querySelectorAll('.assignment'), function (item) {
        if (Number(item.innerHTML === '-')) {
            item.style.backgroundColor = 'yellow';
        } else {
            item.style.backgroundColor = 'white';
        }
    });
}

// Assign EventListener to each <td> with class assignment
window.onload = function addListeners() {
    calculateGrades();
    finishedAssignments();
    let assignment = document.querySelectorAll('.assignment');
    for (let i = 0; i < assignment.length; i++) {
        assignment[i].addEventListener('focusout', validateCell);
        assignment[i].addEventListener('keyup', validateCell);
        assignment[i].addEventListener('keyup', calculateGrades);
        assignment[i].addEventListener('keyup', finishedAssignments);
    }
};

// Automatically removes validateCell when typing into a grade cell
function validateCell() {
    if (!this.innerHTML.match(/^\d*(\.\d*)?$/)) {
        this.innerHTML = this.innerHTML.replace(/[^\d.]/g, '');
    }
    if (parseFloat(this.innerHTML) > 100) {
        this.innerHTML = this.innerHTML.substring(0, 2);
    }
    if (this.innerHTML.trim() === '') {
        this.innerHTML = '-';
    }
    const sel = window.getSelection();
    sel.collapse(this.firstChild, this.innerHTML.length);
}

function finishedAssignments() {
    const table = document.getElementById('grades');
    let count = 0;
    for (let i = 1; i < table.rows.length; i++) {
        for (let j = 2; j < table.rows[i].cells.length - 1; j++) {
            if (table.rows[i].cells[j].innerHTML === '-') {
                count++;
            }
        }
    }
    let AssCount = document.getElementById('AssignmentCount');
    AssCount.innerHTML = AssCount.innerHTML.replace(/\d+/, count.toString());
}

function toCSV() {
    const csv = [], rows = document.querySelectorAll('table tr');
    for (let i = 0; i < rows.length; i++) {
        const row = [], cols = rows[i].querySelectorAll('td, th');

        for (let j = 0; j < cols.length; j++) {
            row.push(cols[j].innerText);
        }
        let delimiter = document.getElementById('delimiter').value;
        if (delimiter.match(/^\s*$/)) {
            delimiter = ',';
        }
        csv.push(row.join(delimiter));
    }
    document.getElementById('csv').innerHTML = csv.join('\n');
}
